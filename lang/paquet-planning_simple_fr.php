<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'planning_simple_description' => 'Créer un planning_simple à partir des évènements d\'un article.',
	'planning_simple_nom' => 'Planning_simple',
	'planning_simple_slogan' => 'Créer un emploi du temps et pouvoir l\'imprimer, rendez-vous sur la page <code>/?page=planning_simple</code>',
);

?>
<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/docker/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
'aucune_donnee' =>'Aucune donnée ne semble disponible pour pouvoir créer le planning !',
'explication_evenements_dependances'=>'Cet article doit avoir des évènements (cf plugin agenda) qui couvre une période d\'une semaine ou moins.',
);

?>

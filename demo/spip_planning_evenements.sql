-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Jeu 25 Septembre 2014 à 20:23
-- Version du serveur: 5.1.72
-- Version de PHP: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Contenu de la table `spip_evenements`
--

INSERT INTO `spip_evenements` (`id_article`, `date_debut`, `date_fin`, `titre`, `lieu`, `horaire`, `id_evenement_source`, `statut`) VALUES
(13, '2014-09-29 08:00:00', '2014-09-29 09:00:00', 'Français', '252', 'oui', 0, 'publie'),
(13, '2014-09-29 11:00:00', '2014-09-29 12:00:00', 'S.E.S', '244', 'oui', 0, 'publie'),
(13, '2014-09-29 13:00:00', '2014-09-29 15:00:00', 'Allemand\r\n', '244', 'oui', 0, 'publie'),
(13, '2014-09-29 15:00:00', '2014-09-29 16:00:00', 'Anglais', '1x/mois\r\n', 'oui', 0, 'publie'),
(13, '2014-09-29 16:00:00', '2014-09-29 17:30:00', 'A.P', '451/B', 'oui', 0, 'publie'),
(13, '2014-09-30 08:00:00', '2014-09-30 10:00:00', 'Anglais', '', 'oui', 0, 'publie'),
(13, '2014-09-30 10:00:00', '2014-09-30 11:00:00', 'Français', '252', 'oui', 0, 'publie'),
(13, '2014-09-30 11:00:00', '2014-09-30 12:30:00', 'SVT Labo', '', 'oui', 0, 'publie'),
(13, '2014-09-30 11:00:00', '2014-09-30 12:30:00', 'PS Labo', '', 'oui', 0, 'publie'),
(13, '2014-09-30 15:00:00', '2014-09-30 16:00:00', 'S.E.S', '452', 'oui', 0, 'publie'),
(13, '2014-10-01 08:00:00', '2014-10-01 10:00:00', 'Français', '252', 'oui', 0, 'publie'),
(13, '2014-10-01 10:00:00', '2014-10-01 11:00:00', 'S.E.S', '413', 'oui', 0, 'publie'),
(13, '2014-10-01 11:00:00', '2014-10-01 12:00:00', 'Maths', '412', 'oui', 0, 'publie'),
(13, '2014-10-02 08:00:00', '2014-10-02 10:00:00', 'S.E.S', '133', 'oui', 0, 'publie'),
(13, '2014-10-02 10:00:00', '2014-10-02 12:00:00', 'H-Géo', '451', 'oui', 0, 'publie'),
(13, '2014-10-02 14:00:00', '2014-10-02 15:00:00', 'Maths', '413', 'oui', 0, 'publie'),
(13, '2014-10-02 15:00:00', '2014-10-02 16:00:00', 'A.P', '/B', 'oui', 0, 'publie'),
(13, '2014-10-02 16:00:00', '2014-10-02 17:00:00', 'TPE', '/A', 'oui', 0, 'publie'),
(13, '2014-10-02 17:00:00', '2014-10-02 18:00:00', 'TPE', 'A', 'oui', 0, 'publie'),
(13, '2014-10-02 17:00:00', '2014-10-02 18:00:00', 'Allemand', '211', 'oui', 0, 'publie'),
(13, '2014-10-03 08:00:00', '2014-10-03 09:00:00', 'ECJS', '433 /B', 'oui', 0, 'publie'),
(13, '2014-10-03 09:00:00', '2014-10-03 10:00:00', 'Maths', '413', 'oui', 0, 'publie'),
(13, '2014-10-03 10:00:00', '2014-10-03 12:00:00', 'EPS', '', 'oui', 0, 'publie'),
(13, '2014-10-03 14:00:00', '2014-10-03 16:00:00', 'H-Géo', '451', 'oui', 0, 'publie'),
(13, '2014-10-03 16:00:00', '2014-10-03 17:00:00', 'A.P', '252/A', 'oui', 0, 'publie');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
